 



# Introduction


NomadBSD is a persistent live system for USB flash drives, based on FreeBSD®. Together with automatic hardware detection and setup, it is configured to be used as a desktop system that works out of the box, but can also be used for data recovery, for educational purposes, or to test FreeBSD®'s hardware compatibility.


# Custom NomadBSD 13.0 and rootfs

Third-party, preinstalled to ada0s2a, with desktop and applications (office,...).

Directly bootable from Grub2.


![](media/nomadbsd-desktop.png)


# MBR Legacy Harddisk

The regular NomadBSD installation will be persistent on the harddisk.
However, it will use EFI. On some notebooks, EFI is not possible.

So this method allows to use a regular MBR.
I personally prefer MBR for the home desktop.

MBR works, it is simple, it is reliable.


# Installation 


````
newfs ada0s2a
mount /dev/ada0s2a /mnt
cd /mnt
fetch -R https://gitlab.com/openbsd98324/nomadbsd-rootfs/-/raw/main/pub/release/130R-20210508/amd64/1644537436-2-nomadbsd-13-p4-disk-rootfs-v2.tar.gz
tar xvpfz 1644537436-2-nomadbsd-13-p4-disk-rootfs-v2.tar.gz
sync
echo the workaround for eeepc 
echo screen.textmode=0 >> boot/loader.conf
````

# Grub 2

````
menuentry 'NomadBSD Chainloader (msdos2)' {
   set root='hd0,msdos2'
   chainloader +1
}
````





# Freebsd Boot Loader


In case, it shows no graphics, at boot, press '3' and type:  
 
````
   vbe set 0x17e 
   boot 
````


# /boot/loader.conf on Intel (eeepc asus,...) notebooks

The /boot/loader.conf is modified, and it has the following line for intel (eeepc notebooks):   screen.textmode=0   

See: 
https://gitlab.com/openbsd98324/nomadbsd-rootfs/-/blob/main/config/notebook/asus/eeepc-intel/boot/loader.conf



# Default User
 
Login: netbsd
Password: root

## Get Root?

````
sudo su
````

# References.

https://nomadbsd.org/

